public class TestBug {

    public TestBug () {

        // Below commented line behaves as expected
        // Map<String,Account> mapAccount = new Map<String,Account> ([SELECT Id, Name FROM Account LIMIT 100]);

        // This line should give Compile/Runtime time error.
        //Map<String,Account> mapAccount = new Map<String,Account> ([SELECT Id, Name FROM Account].Name);
    }
}
